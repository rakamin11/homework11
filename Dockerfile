# Menggunakan OS node alpine pada saat docker dijalankan di container
FROM node:18.14.2-alpine3.17

# Menentukan folder apa yang akan di set di docker
WORKDIR /app

# Copy package.json dan package-lock.json ke dalam folder docker
COPY package*.json ./

# RUN npm install
RUN npm install

# Copy seluruh folder ke dalam folder docker
COPY . .

EXPOSE 8081

# Jalankan perintah node index.js
CMD npm start