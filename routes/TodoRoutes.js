const express = require('express')
const router = express.Router()
const todoController = require('../controllers/TodoController')

router.use('/api', todoController)

// export this router to use in our index.js
module.exports = router