const dotenv = require('dotenv');
dotenv.config()
const app = require('./app')
const PORT = 8081 || process.env.PORT;

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})