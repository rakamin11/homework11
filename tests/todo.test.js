const request = require('supertest')
const assert = require('assert')
const dotenv = require('dotenv')
dotenv.config()

const app = require('../app')

// Test Scenario
describe('Todo Unit Testing', () => {
    // Test Case
    it('Get Ping, should return 200 ok', () => {
        request(app)
            .get('/ping')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .end((err, res) => {
                if (err) throw err
            })
    })
})

describe('GET Testing', () => {
    // GET /api/todos
    it('Get All Todo, should return 200 ok', () => {
        request(app)
            .get('/api/todos')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .end((err, res) => {
                if (err) throw err
            })
    })


    // GET /api/todos/:id
    it('Get Todo By Id, should return 200 ok', () => {
        request(app)
            .post('/api/todos')
            .set({
                title: 'Makanan/Minuman',
                description: 'Deskripsikan rasa dari makanan/minuman yang ada.'
            })
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(201)
            .end((err, res) => {
                if (err) throw err
            })

        request(app)
            .get('/api/todos/1')
            .expect('Content-Type', 'application/json; charset=ut-8')
            .expect(200)
            .end((err, res) => {
                if (err) throw err
            })
    })

    it('Data Todo should return 404 Not Found', () => {
        request(app)
            .get('/api/todos/100')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(404)
            .end((err, res) => {
                if (err) throw err
            })
    })
})

// POST /api/todos
describe('POST Testing', () => {
    it('Create Data Todo, should return 201 ok', () => {
        request(app)
            .post('/api/todos')
            .set({
                title: 'Makanan/Minuman',
                description: 'Deskripsikan rasa dari makanan/minuman yang ada.'
            })
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(201)
            .end((err, res) => {
                if (err) throw err
            })
    })
})

// PUT /api/todos/:id
describe('PUT Testing', () => {
    it('Update Data Todo, should return 201 ok', () => {
        request(app)
            .get('/api/todos/1')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .end((err, res) => {
                if (err) throw err
            })

        request(app)
            .put('/api/todos/1')
            .set({
                title: 'Makanan/Minuman',
                description: 'Deskripsikan rasa dari makanan/minuman yang ada.'
            })
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(201)
            .end((err, res) => {
                if (err) throw err
            })
    })

    it('Data Todo should return 404 Not Found', () => {
        request(app)
            .get('/api/todos/100')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(404)
            .end((err, res) => {
                if (err) throw err
            })
    })
})

// DELETE /api/todos/:id
describe('DELETE Testing', () => {
    it('Delete Data Todo, should return 200 ok', () => {
        request(app)
            .delete('/api/todos/1')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .end((err, res) => {
                if (err) throw err
            })
    })

    it('Data Todo should return 404 Not Found', () => {
        request(app)
            .get('/api/todos/100')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(404)
            .end((err, res) => {
                if (err) throw err
            })
    })
})