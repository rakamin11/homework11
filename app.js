const express = require('express');
const morgan = require('morgan');
const app = express();
const formidableMiddleware = require('express-formidable');
const todoRoutes = require('./routes/TodoRoutes')

app.use(formidableMiddleware())
app.use(morgan('combined'))
app.use(todoRoutes)


app.get('/ping', (req, res) => {
    res.json({
        ping: true
    })
});

module.exports = app