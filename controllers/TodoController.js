const express = require('express')
const router = express.Router()
const todoService = require('../services/TodoService')

router.get('/todos', async (req, res) => {
    try {
        const todo = await todoService.findAllTodo()

        res.status(200).json({
            data: todo
        })

    } catch (error) {
        res.status(500).json({
            error: 'Failed to Fetch Data.'
        })
    }
})

router.get('/todos/:id', async (req, res) => {
    try {
        const todo = await todoService.findTodoById(req, res)

        if (!todo) {
            res.status(404).json({
                message: 'Data Todo not found.'
            })
        } else {
            res.status(200).json({
                data: todo
            })
        }

    } catch (error) {
        res.status(500).json({
            error: 'Failed to Fetch Data.'
        })
    }
})

router.post('/todos', async (req, res) => {
    try {
        const todo = await todoService.createNewTodo(req, res)

        res.status(201).json({
            message: "Create todos successfully",
            data: todo
        })

    } catch (error) {
        res.status(500).json({
            error: 'Failed to Fetch Data.'
        })
    }
})

router.put('/todos/:id', async (req, res) => {
    try {
        const todo = await todoService.findTodoById(req, res)

        if (!todo) {
            res.status(404).json({
                message: 'Todo not found'
            })
        } else {
            await todoService.updateTodo(req, res)

            res.status(200).json({
                message: "Todos updated successfully."
            })
        }

    } catch (error) {
        res.status(500).json({
            error: 'Failed to Fetch Data.'
        })
    }
})

router.delete('/todos/:id', async (req, res) => {
    try {
        const todo = await todoService.findTodoById(req, res)

        if (!todo) {
            res.status(404).json({
                message: 'Todo not found'
            })
        } else {
            await todoService.deleteTodo(req, res)

            res.status(200).json({
                message: "Todos deleted successfully"
            })
        }

    } catch (error) {
        res.status(500).json({
            error: 'Failed to Fetch Data.'
        })
    }
})

module.exports = router